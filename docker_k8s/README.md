#Initialize the Kubernetes Cluster

 TESTING

WIP
Create a new Job Template with the following:

    Name is kubernetes-init
    Inventory is kubernetes-cluster
    Project is kubernetes-cluster
    Playbook is /ansible/docker_kuber.yml. This playbook:
        runs kubeadm reset to clean up any existing cluster & cleans existing kube config
        runs kubeadm init on controller & sets up kube config for ubuntu user
        installs calico cni for pod networking
        runs kubeadm join on worker
